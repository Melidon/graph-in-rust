use {
    super::Graph,
    std::ops::{Index, IndexMut},
};

struct Matrix<T> {
    values: Vec<Vec<T>>,
}

impl<T> Matrix<T> {
    fn new() -> Self {
        Matrix { values: Vec::new() }
    }

    fn resize_with<F: Copy>(&mut self, new_rows: usize, new_columns: usize, f: F)
    where
        F: FnMut() -> T,
    {
        for row in &mut self.values {
            row.resize_with(new_columns, f);
        }
        self.values.resize_with(new_rows, || {
            let mut row = Vec::with_capacity(new_columns);
            row.resize_with(new_columns, f);
            row
        });
    }
}

impl<T> Index<(usize, usize)> for Matrix<T> {
    type Output = T;

    fn index(&self, (row, col): (usize, usize)) -> &Self::Output {
        &self.values[row][col]
    }
}

impl<T> IndexMut<(usize, usize)> for Matrix<T> {
    fn index_mut(&mut self, (row, col): (usize, usize)) -> &mut Self::Output {
        &mut self.values[row][col]
    }
}

pub struct AdjacencyMatrixGraph<VertexData, EdgeData> {
    vertices: Vec<VertexData>,
    edges: Matrix<Option<EdgeData>>,
}

impl<VertexData, EdgeData> Graph<VertexData, EdgeData>
    for AdjacencyMatrixGraph<VertexData, EdgeData>
{
    type VertexId = usize;
    type EdgeId = (Self::VertexId, Self::VertexId);

    fn new() -> Self {
        Self {
            vertices: Vec::new(),
            edges: Matrix::new(),
        }
    }

    fn add_vertex(&mut self, data: VertexData) -> Self::VertexId {
        let id = self.vertices.len();
        self.vertices.push(data);
        self.edges.resize_with(id + 1, id + 1, || None);
        id
    }

    fn get_vertex_data(&self, vertex_id: Self::VertexId) -> &VertexData {
        &self.vertices[vertex_id]
    }

    fn get_vertex_edges(&self, vertex_id: Self::VertexId) -> impl Iterator<Item = Self::EdgeId> {
        self.edges.values[vertex_id]
            .iter()
            .enumerate()
            .filter(|(_, edge)| edge.is_some())
            .map(move |(col, _)| (vertex_id, col))
    }

    fn add_edge(
        &mut self,
        from: Self::VertexId,
        to: Self::VertexId,
        data: EdgeData,
    ) -> Self::EdgeId {
        self.edges[(from, to)] = Some(data);
        (from, to)
    }

    fn get_edge_from(&self, edge_id: Self::EdgeId) -> Self::VertexId {
        let (from, _) = edge_id;
        from
    }

    fn get_edge_to(&self, edge_id: Self::EdgeId) -> Self::VertexId {
        let (_, to) = edge_id;
        to
    }

    fn get_edge_data(&self, edge_id: Self::EdgeId) -> &EdgeData {
        self.edges[edge_id].as_ref().unwrap()
    }
}
