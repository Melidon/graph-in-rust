use super::Graph;

pub struct AdjacencyListGraph<VertexData, EdgeData> {
    vertices: Vec<Vertex<VertexData, EdgeData>>,
    edges: Vec<Edge<VertexData, EdgeData>>,
}

struct Vertex<VertexData, EdgeData> {
    data: VertexData,
    edges: Vec<<AdjacencyListGraph<VertexData, EdgeData> as Graph<VertexData, EdgeData>>::EdgeId>,
}

struct Edge<VertexData, EdgeData> {
    from: <AdjacencyListGraph<VertexData, EdgeData> as Graph<VertexData, EdgeData>>::VertexId,
    to: <AdjacencyListGraph<VertexData, EdgeData> as Graph<VertexData, EdgeData>>::VertexId,
    data: EdgeData,
}

impl<VertexData, EdgeData> Graph<VertexData, EdgeData>
    for AdjacencyListGraph<VertexData, EdgeData>
{
    type VertexId = usize;
    type EdgeId = usize;

    fn new() -> Self {
        Self {
            vertices: Vec::new(),
            edges: Vec::new(),
        }
    }

    fn add_vertex(&mut self, data: VertexData) -> Self::VertexId {
        let vertex_id = self.vertices.len();
        self.vertices.push(Vertex {
            data,
            edges: Vec::new(),
        });
        vertex_id
    }

    fn get_vertex_data(&self, vertex_id: Self::VertexId) -> &VertexData {
        &self.vertices[vertex_id].data
    }

    fn get_vertex_edges(&self, vertex_id: Self::VertexId) -> impl Iterator<Item = Self::EdgeId> {
        self.vertices[vertex_id].edges.iter().copied()
    }

    fn add_edge(
        &mut self,
        from: Self::VertexId,
        to: Self::VertexId,
        data: EdgeData,
    ) -> Self::EdgeId {
        let edge_id = self.edges.len();
        self.edges.push(Edge { from, to, data });
        self.vertices[from].edges.push(edge_id);
        edge_id
    }

    fn get_edge_from(&self, edge_id: Self::EdgeId) -> Self::VertexId {
        self.edges[edge_id].from
    }

    fn get_edge_to(&self, edge_id: Self::EdgeId) -> Self::VertexId {
        self.edges[edge_id].to
    }

    fn get_edge_data(&self, edge_id: Self::EdgeId) -> &EdgeData {
        &self.edges[edge_id].data
    }
}
