use graph::{AdjacencyListGraph, AdjacencyMatrixGraph, Graph, Search};

fn create_map<G: Graph<String, f64>>() -> G {
    let mut map = G::new();

    let annavölgy = map.add_vertex(String::from("Annavölgy"));
    let bajna = map.add_vertex(String::from("Bajna"));
    let csolnok = map.add_vertex(String::from("Csolnok"));
    let dorog = map.add_vertex(String::from("Dorog"));
    let dág = map.add_vertex(String::from("Dág"));
    let ebszőnybánya = map.add_vertex(String::from("Ebszőnybánya"));
    let epöl = map.add_vertex(String::from("Epöl"));
    let gyermely = map.add_vertex(String::from("Gyermely"));
    let leányvár = map.add_vertex(String::from("Leányvár"));
    let máriahalom = map.add_vertex(String::from("Máriahalom"));
    let nagysáp = map.add_vertex(String::from("Nagysáp"));
    let perbál = map.add_vertex(String::from("Perbál"));
    let piliscsaba = map.add_vertex(String::from("Piliscsaba"));
    let pilisjászfalu = map.add_vertex(String::from("Pilisjászfalu"));
    let szomor = map.add_vertex(String::from("Szomor"));
    let sárisáp = map.add_vertex(String::from("Sárisáp"));
    let tinnye = map.add_vertex(String::from("Tinnye"));
    let tokod = map.add_vertex(String::from("Tokod"));
    let tokodaltáró = map.add_vertex(String::from("Tokodaltáró"));
    let tát = map.add_vertex(String::from("Tát"));
    let tök = map.add_vertex(String::from("Tök"));
    let zsámbék = map.add_vertex(String::from("Zsámbék"));
    let úny = map.add_vertex(String::from("Úny"));

    map.add_edge(annavölgy, ebszőnybánya, 1.8583770612336594);
    map.add_edge(annavölgy, sárisáp, 2.676967574040747);
    map.add_edge(bajna, epöl, 3.150574338004166);
    map.add_edge(bajna, nagysáp, 3.1808150439909393);
    map.add_edge(bajna, szomor, 8.60213296661599);
    map.add_edge(csolnok, dorog, 3.254963826721517);
    map.add_edge(csolnok, dág, 3.6103721273270875);
    map.add_edge(csolnok, leányvár, 4.261490493395955);
    map.add_edge(dorog, csolnok, 3.254963826721517);
    map.add_edge(dorog, tokodaltáró, 3.1062031387417828);
    map.add_edge(dorog, leányvár, 5.412071398892213);
    map.add_edge(dág, csolnok, 3.6103721273270875);
    map.add_edge(dág, máriahalom, 3.9187054967026045);
    map.add_edge(dág, sárisáp, 3.2094390718504413);
    map.add_edge(dág, úny, 2.5602385256622737);
    map.add_edge(ebszőnybánya, annavölgy, 1.8583770612336594);
    map.add_edge(ebszőnybánya, nagysáp, 3.882325450153554);
    map.add_edge(ebszőnybánya, tokod, 2.2680846718707253);
    map.add_edge(ebszőnybánya, tát, 3.9493903669509858);
    map.add_edge(epöl, bajna, 3.150574338004166);
    map.add_edge(epöl, máriahalom, 5.694116990397191);
    map.add_edge(gyermely, szomor, 1.5284188213716778);
    map.add_edge(leányvár, csolnok, 4.261490493395955);
    map.add_edge(leányvár, dorog, 5.412071398892213);
    map.add_edge(leányvár, pilisjászfalu, 3.494692363742216);
    map.add_edge(máriahalom, dág, 3.9187054967026045);
    map.add_edge(máriahalom, epöl, 5.694116990397191);
    map.add_edge(máriahalom, szomor, 5.220237347161415);
    map.add_edge(máriahalom, sárisáp, 5.581868238768785);
    map.add_edge(máriahalom, úny, 2.8205406654286582);
    map.add_edge(nagysáp, bajna, 3.1808150439909393);
    map.add_edge(nagysáp, ebszőnybánya, 3.882325450153554);
    map.add_edge(nagysáp, tokod, 6.023145466869913);
    map.add_edge(nagysáp, tát, 7.302900314078783);
    map.add_edge(perbál, tinnye, 3.565636953274589);
    map.add_edge(perbál, tök, 3.6615017848063363);
    map.add_edge(piliscsaba, pilisjászfalu, 3.4013935689552652);
    map.add_edge(piliscsaba, tinnye, 3.8650789107854706);
    map.add_edge(pilisjászfalu, leányvár, 3.494692363742216);
    map.add_edge(pilisjászfalu, piliscsaba, 3.4013935689552652);
    map.add_edge(pilisjászfalu, tinnye, 3.8857725628516295);
    map.add_edge(szomor, bajna, 8.60213296661599);
    map.add_edge(szomor, gyermely, 1.5284188213716778);
    map.add_edge(szomor, máriahalom, 5.220237347161415);
    map.add_edge(szomor, zsámbék, 6.289861869792834);
    map.add_edge(sárisáp, annavölgy, 2.676967574040747);
    map.add_edge(sárisáp, dág, 3.2094390718504413);
    map.add_edge(sárisáp, máriahalom, 5.581868238768785);
    map.add_edge(sárisáp, úny, 5.556718225704513);
    map.add_edge(tinnye, perbál, 3.565636953274589);
    map.add_edge(tinnye, piliscsaba, 3.8650789107854706);
    map.add_edge(tinnye, pilisjászfalu, 3.8857725628516295);
    map.add_edge(tinnye, úny, 3.821741669563625);
    map.add_edge(tokod, ebszőnybánya, 2.2680846718707253);
    map.add_edge(tokod, nagysáp, 6.023145466869913);
    map.add_edge(tokod, tokodaltáró, 2.713366410658262);
    map.add_edge(tokod, tát, 1.9280020129596076);
    map.add_edge(tokodaltáró, dorog, 3.1062031387417828);
    map.add_edge(tokodaltáró, tokod, 2.713366410658262);
    map.add_edge(tokodaltáró, tát, 2.9945839724099184);
    map.add_edge(tát, ebszőnybánya, 3.9493903669509858);
    map.add_edge(tát, nagysáp, 7.302900314078783);
    map.add_edge(tát, tokod, 1.9280020129596076);
    map.add_edge(tát, tokodaltáró, 2.9945839724099184);
    map.add_edge(tök, perbál, 3.6615017848063363);
    map.add_edge(tök, zsámbék, 2.087878579853077);
    map.add_edge(zsámbék, szomor, 6.289861869792834);
    map.add_edge(zsámbék, tök, 2.087878579853077);
    map.add_edge(úny, dág, 2.5602385256622737);
    map.add_edge(úny, máriahalom, 2.8205406654286582);
    map.add_edge(úny, sárisáp, 5.556718225704513);
    map.add_edge(úny, tinnye, 3.821741669563625);

    map
}

fn main() {
    let map = create_map::<AdjacencyListGraph<String, f64>>();
    for (vertex_id, depth) in map.bfs(0) {
        print!("{}", "  ".repeat(depth));
        println!("{}", map.get_vertex_data(vertex_id));
    }
    println!();
    for (vertex_id, depth) in map.dfs(0) {
        print!("{}", "  ".repeat(depth));
        println!("{}", map.get_vertex_data(vertex_id));
    }

    let map = create_map::<AdjacencyMatrixGraph<String, f64>>();
    for (vertex_id, depth) in map.bfs(0) {
        print!("{}", "  ".repeat(depth));
        println!("{}", map.get_vertex_data(vertex_id));
    }
    println!();
    for (vertex_id, depth) in map.dfs(0) {
        print!("{}", "  ".repeat(depth));
        println!("{}", map.get_vertex_data(vertex_id));
    }
}
