use {
    crate::Graph,
    std::{
        collections::{HashSet, VecDeque},
        marker::PhantomData,
    },
};

pub trait Search<VertexData, EdgeData>: Graph<VertexData, EdgeData>
where
    Self: std::marker::Sized,
{
    fn bfs(&self, start: Self::VertexId) -> SearchState<'_, VertexData, EdgeData, Self, PopFront>;
    fn dfs(&self, start: Self::VertexId) -> SearchState<'_, VertexData, EdgeData, Self, PopBack>;
}

impl<VertexData, EdgeData, G: Graph<VertexData, EdgeData>> Search<VertexData, EdgeData> for G {
    fn bfs(&self, start: Self::VertexId) -> SearchState<'_, VertexData, EdgeData, Self, PopFront> {
        SearchState::<VertexData, EdgeData, Self, PopFront> {
            graph: self,
            visited: HashSet::new(),
            queue: VecDeque::from([(start, 0)]),
            pop: PhantomData,
        }
    }
    fn dfs(&self, start: Self::VertexId) -> SearchState<'_, VertexData, EdgeData, Self, PopBack> {
        SearchState::<VertexData, EdgeData, Self, PopBack> {
            graph: self,
            visited: HashSet::new(),
            queue: VecDeque::from([(start, 0)]),
            pop: PhantomData,
        }
    }
}

pub struct SearchState<
    'a,
    VertexData,
    EdgeData,
    G: Graph<VertexData, EdgeData>,
    Pop: PopStrategy<(G::VertexId, usize)>,
> {
    graph: &'a G,
    visited: HashSet<G::VertexId>,
    queue: VecDeque<(G::VertexId, usize)>,
    pop: PhantomData<Pop>,
}

impl<
        'a,
        VertexData,
        EdgeData,
        G: Graph<VertexData, EdgeData>,
        Pop: PopStrategy<(G::VertexId, usize)>,
    > Iterator for SearchState<'a, VertexData, EdgeData, G, Pop>
{
    type Item = (G::VertexId, usize);

    fn next(&mut self) -> Option<Self::Item> {
        while let Some((vertex_id, depth)) = Pop::POP(&mut self.queue) {
            if self.visited.contains(&vertex_id) {
                continue;
            }
            for neighbour_vertex_id in self
                .graph
                .get_vertex_edges(vertex_id)
                .map(|edge_id| self.graph.get_edge_to(edge_id))
            {
                self.queue.push_back((neighbour_vertex_id, depth + 1));
            }
            self.visited.insert(vertex_id);
            return Some((vertex_id, depth));
        }
        None
    }
}

pub trait PopStrategy<T> {
    const POP: fn(&mut VecDeque<T>) -> Option<T>;
}

pub struct PopFront;

impl<T> PopStrategy<T> for PopFront {
    const POP: fn(&mut VecDeque<T>) -> Option<T> = VecDeque::pop_front;
}

pub struct PopBack;

impl<T> PopStrategy<T> for PopBack {
    const POP: fn(&mut VecDeque<T>) -> Option<T> = VecDeque::pop_back;
}
