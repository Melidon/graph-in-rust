mod adjacency_list_graph;
mod adjacency_matrix_graph;
pub use {adjacency_list_graph::AdjacencyListGraph, adjacency_matrix_graph::AdjacencyMatrixGraph};

use std::hash::Hash;

pub trait Graph<VertexData, EdgeData> {
    type VertexId: Eq + Hash + Copy;
    type EdgeId: Eq + Hash + Copy;

    fn new() -> Self;

    fn add_vertex(&mut self, data: VertexData) -> Self::VertexId;

    fn get_vertex_data(&self, vertex_id: Self::VertexId) -> &VertexData;

    fn get_vertex_edges(&self, vertex_id: Self::VertexId) -> impl Iterator<Item = Self::EdgeId>;

    fn add_edge(
        &mut self,
        from: Self::VertexId,
        to: Self::VertexId,
        data: EdgeData,
    ) -> Self::EdgeId;

    fn get_edge_from(&self, edge_id: Self::EdgeId) -> Self::VertexId;

    fn get_edge_to(&self, edge_id: Self::EdgeId) -> Self::VertexId;

    fn get_edge_data(&self, edge_id: Self::EdgeId) -> &EdgeData;
}
