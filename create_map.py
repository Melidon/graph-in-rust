from geopy.geocoders import Nominatim
from geopy.distance import geodesic


def calculate_distance(origin, destination):
    geolocator = Nominatim(
        user_agent='Please specify a custom `user_agent` with `Nominatim(user_agent="my-application")`'
    )

    location1 = geolocator.geocode(origin)
    location2 = geolocator.geocode(destination)

    loc1 = (location1.latitude, location1.longitude)
    loc2 = (location2.latitude, location2.longitude)

    return geodesic(loc1, loc2).km


roads: list[tuple[str, list[str]]] = [
    ("Annavölgy", ["Ebszőnybánya", "Sárisáp"]),
    ("Bajna", ["Epöl", "Nagysáp", "Szomor"]),
    ("Csolnok", ["Dorog", "Dág", "Leányvár"]),
    ("Dorog", ["Csolnok", "Tokodaltáró", "Leányvár"]),
    ("Dág", ["Csolnok", "Máriahalom", "Sárisáp", "Úny"]),
    ("Ebszőnybánya", ["Annavölgy", "Nagysáp", "Tokod", "Tát"]),
    ("Epöl", ["Bajna", "Máriahalom"]),
    ("Gyermely", ["Szomor"]),
    ("Leányvár", ["Csolnok", "Dorog", "Pilisjászfalu"]),
    ("Máriahalom", ["Dág", "Epöl", "Szomor", "Sárisáp", "Úny"]),
    ("Nagysáp", ["Bajna", "Ebszőnybánya", "Tokod", "Tát"]),
    ("Perbál", ["Tinnye", "Tök"]),
    ("Piliscsaba", ["Pilisjászfalu", "Tinnye"]),
    ("Pilisjászfalu", ["Leányvár", "Piliscsaba", "Tinnye"]),
    ("Szomor", ["Bajna", "Gyermely", "Máriahalom", "Zsámbék"]),
    ("Sárisáp", ["Annavölgy", "Dág", "Máriahalom", "Úny"]),
    ("Tinnye", ["Perbál", "Piliscsaba", "Pilisjászfalu", "Úny"]),
    ("Tokod", ["Ebszőnybánya", "Nagysáp", "Tokodaltáró", "Tát"]),
    ("Tokodaltáró", ["Dorog", "Tokod", "Tát"]),
    ("Tát", ["Ebszőnybánya", "Nagysáp", "Tokod", "Tokodaltáró"]),
    ("Tök", ["Perbál", "Zsámbék"]),
    ("Zsámbék", ["Szomor", "Tök"]),
    ("Úny", ["Dág", "Máriahalom", "Sárisáp", "Tinnye"]),
]

for town, _ in roads:
    print(f'    let {town.lower()} = map.add_vertex(String::from("{town}"));')

print()

for origin, destinations in roads:
    for destination in destinations:
        print(
            f"    map.add_edge({origin.lower()}, {destination.lower()}, {calculate_distance(origin, destination)});"
        )
